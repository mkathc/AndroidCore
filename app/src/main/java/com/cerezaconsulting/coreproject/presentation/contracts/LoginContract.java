package com.cerezaconsulting.coreproject.presentation.contracts;

import com.cerezaconsulting.coreproject.core.BasePresenter;
import com.cerezaconsulting.coreproject.core.BaseView;

/**
 * Created by katherine on 12/05/17.
 */

public interface LoginContract {
    interface View extends BaseView<Presenter> {
        //FUNCIONES QUE SE HEREDAN EN EL FRAGMENT
        void loginSucessful();
        boolean isActive();
    }

    interface Presenter extends BasePresenter {
        //FUNCIONES QUE HEREDA EL PRESENTER
        void loginUser(String username, String password);

    }
}
