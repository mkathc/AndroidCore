package com.cerezaconsulting.coreproject.presentation.contracts;


import com.cerezaconsulting.coreproject.core.BasePresenter;
import com.cerezaconsulting.coreproject.core.BaseView;

/**
 * Created by katherine on 3/05/17.
 */

public interface RegisterContract {
    interface View extends BaseView<Presenter> {
        void registerSucessful();
        boolean isActive();
    }

    interface Presenter extends BasePresenter {
        void registerUser();

    }
}
